﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public List<MovementBehaviour> MovementBehaviours;
    public List<RotationBehaviour> RotationBehaviours;

    public delegate void del();
    public event del DieEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitializeLists()
    {
        MovementBehaviours = new List<MovementBehaviour>();
        RotationBehaviours = new List<RotationBehaviour>();
    }

    public void Die()
    {
        foreach (MovementBehaviour b in MovementBehaviours)
            b.Stop();
        foreach (RotationBehaviour b in RotationBehaviours)
            b.Stop();

        DieEvent?.Invoke();
    }
}
