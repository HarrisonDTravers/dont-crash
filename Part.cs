﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : MonoBehaviour
{
    public int length;
    public GameObject Background;
    public GameObject Colliders;
    public Transform StartPoint;
    public Transform EndPoint;
    public bool canFlipVertical;
    public bool canFlipHorizontal;
    public bool IsFlippedVertical { get; set; }
    public bool IsFlippedHorizontal { get; set; }

    public void FlipVertical()
    {
        transform.localScale = new Vector3(transform.localScale.x, -transform.localScale.y, transform.localScale.z);
        IsFlippedVertical = !IsFlippedVertical;
        if (IsFlippedVertical)
            gameObject.name += "_VertFlip";
    }

    public void FlipHorizontal()
    {
        transform.localScale = new Vector3(-transform.localScale.x , transform.localScale.y, transform.localScale.z);
        Transform oldStart = StartPoint;
        StartPoint = EndPoint;
        EndPoint = oldStart;
        IsFlippedHorizontal = !IsFlippedHorizontal;
        if (IsFlippedHorizontal)
            gameObject.name += "_HorizFlip";
    }
}
