﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolTranslator : MonoBehaviour
{
    public enum PatrolOrderEnum { Ascending, Descending, Random }
    public PatrolOrderEnum PatrolOrder;
    public List<Transform> patrolPoints;
    public bool randomStartPosition;
    public float speed;
    public float distanceBeforePatrolPointChange;
    
    private int TargetIndex { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        if (randomStartPosition)
        {
            Vector3 startPosition;
            int targetIndex;
            GetRandomStartPosition(out startPosition, out targetIndex);
            transform.position = startPosition;
            TargetIndex = targetIndex;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, patrolPoints[TargetIndex].position, speed);
        if(Vector3.Distance(transform.position, patrolPoints[TargetIndex].position) < distanceBeforePatrolPointChange)
            switch (PatrolOrder)
            {
                case PatrolOrderEnum.Ascending:
                    TargetIndex = (TargetIndex + 1) % patrolPoints.Count;
                    break;
                case PatrolOrderEnum.Descending:
                    TargetIndex = (patrolPoints.Count + TargetIndex - 1) % patrolPoints.Count;
                    break;
                case PatrolOrderEnum.Random:
                    TargetIndex = Random.Range(0, patrolPoints.Count);
                    break;
            }
    }

    public void GetRandomStartPosition(out Vector3 startPosition, out int edgeEndIndex)
    {
        int edgeStartIndex = 0;
        edgeEndIndex = 0;
        if (patrolPoints.Count > 2)
        {
            edgeStartIndex = Random.Range(0, patrolPoints.Count);
            if (Random.Range(0, 2) == 0)
                edgeEndIndex = (patrolPoints.Count + edgeStartIndex - 1) % patrolPoints.Count;
            else
                edgeEndIndex = (edgeEndIndex + 1) % patrolPoints.Count;
        }
        else if (patrolPoints.Count == 2)
        {
            edgeStartIndex = 0;
            edgeEndIndex = 1;
        }

        startPosition = RandomBetweenTwoPoints(patrolPoints[edgeStartIndex].position, patrolPoints[edgeEndIndex].position);
    }

    public Vector3 RandomBetweenTwoPoints(Vector3 v1, Vector3 v2)
    {
        return Vector2.Lerp(v1, v2, Random.Range(0f, 1f));
    }
}
