﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RotationBehaviour
{
    public GameObject GameObject { get; set; }

    public RotationBehaviour(GameObject gameObject)
    {
        GameObject = gameObject;
    }

    public abstract void Rotate();
    public abstract void Stop();
}

public class FaceVelocityRotationBehaviour : RotationBehaviour
{
    public Rigidbody2D rBody;
    public bool IsRotating;
    public bool IsContinuous;

    public FaceVelocityRotationBehaviour(GameObject gameObject, bool isContinuous) : base(gameObject)
    {
        if ((rBody = gameObject.GetComponent<Rigidbody2D>()) == null)
            rBody = gameObject.AddComponent<Rigidbody2D>();
        IsContinuous = isContinuous;
    }    

    public override void Rotate()
    {
        if (IsContinuous)
        {
            IsRotating = true;
            CoroutineSingleton.Instance.StartCoroutine(RecurringRotation());
        }
        else
        {
            Vector2 dir = rBody.velocity;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            GameObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    public override void Stop()
    {
        IsRotating = false;
    }

    public IEnumerator RecurringRotation()
    {
        while (IsRotating)
        {
            Vector2 dir = rBody.velocity;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            GameObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            yield return new WaitForEndOfFrame();
        }
    }
}
