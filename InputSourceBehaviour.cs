﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputSourceBehaviour
{
    public abstract void CheckForInput();
}

public class InputTouchBehaviour : InputSourceBehaviour
{
    public delegate void Del();
    public event Del TouchBeginEvent;
    public event Del TouchEndEvent;

    public override void CheckForInput()
    {
        if (Input.touchCount>0)
        {
            switch(Input.GetTouch(0).phase)
            {
                case TouchPhase.Began:
                    TouchBeginEvent?.Invoke();
                    Debug.Log("Player : Touch");
                    break;
                case TouchPhase.Ended:
                    TouchEndEvent?.Invoke();
                    Debug.Log("Player : Untouch");
                    break;
            }
        }
        else if(Input.GetMouseButtonDown(0))
            TouchBeginEvent?.Invoke();
        else if(Input.GetMouseButtonUp(0))
            TouchEndEvent?.Invoke();

    }
}