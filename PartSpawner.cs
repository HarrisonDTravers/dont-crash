﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartSpawner
{
    public List<Part> SpawnedParts { get; set; }
    private Grid Grid { get; set; }

    public PartSpawner(Grid grid)
    {
        SpawnedParts = new List<Part>();
        Grid = grid;
    }

    public void FirstSpawn(Part partReference)
    {
        Part newPart = MonoBehaviour.Instantiate<Part>(partReference);
        newPart.transform.position = Vector3.zero;
        newPart.transform.SetParent(Grid.transform);
        SpawnedParts.Add(newPart);
    }

    public void Spawn(Part partReference)
    {
        Part newPart = MonoBehaviour.Instantiate<Part>(partReference);
        if (newPart.canFlipHorizontal && Random.Range(0, 2) > 0)
            newPart.FlipHorizontal();
        if (newPart.canFlipVertical && Random.Range(0, 2) > 0)
            newPart.FlipVertical();

        newPart.transform.position = (newPart.transform.position-newPart.StartPoint.position) + GetLastPart().EndPoint.position;
        newPart.transform.SetParent(Grid.transform);
        SpawnedParts.Add(newPart);
    }

    public Part GetLastPart()
    {
        return SpawnedParts[SpawnedParts.Count - 1];
    }
}
