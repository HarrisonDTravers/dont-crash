﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLoader : MonoBehaviour
{
    public static GameLoader Instance { get; set; }
    public Sprite playerSprite;    
    public Part startingPart;    
    public int spawnNextPartDistance = 10;
    public int maxActiveParts = 30;
    public Grid grid;
    public new SmoothCamera camera;
    public float maxDeathWallDistance = 5;
    public InputHandler inputHandler;
    public Text text_Distance;
    public Text text_BestDistance;
    public Text text_BestDistanceToday;


    private Entity player;
    private PartSpawner partSpawner;
    private InputTouchBehaviour input;
    private InputRecorder recorder;
    private FaceVelocityRotationBehaviour costantRotationBehaviour;
    private Part[] parts;
    private GameObject DeathWallPrefab { get; set; }
    private Entity DeathWall { get; set; }
    private GhostRecording BestGhost { get; set; }
    private Entity GhostEntity { get; set; }

    //UI
    public Canvas canvas;
    public Text heading;

    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("HighScore-" + System.DateTime.Today.Year + "-" + System.DateTime.Today.DayOfYear, 0);
        Instance = this;
        parts = Resources.LoadAll<Part>("RandomlySelectableParts");
        DeathWallPrefab = Resources.Load<GameObject>("DeathWall");
        heading.text = "Don't Crash";
        text_BestDistance.text = "Best " + PlayerPrefs.GetInt("HighScore") + " m";
        text_BestDistanceToday.text = "Today's Best " + PlayerPrefs.GetInt("HighScore-" + System.DateTime.Today.Year + "-" + System.DateTime.Today.DayOfYear) + " m";
        

        

        input = new InputTouchBehaviour();
        inputHandler.SourceBehaviour = input;
        recorder = new InputRecorder();
        input.TouchBeginEvent += recorder.Touch;
        input.TouchEndEvent += recorder.Untouch;
        GenerateWorld();
    }

    public void GenerateWorld()
    {
        foreach (Transform g in grid.transform)
            Destroy(g.gameObject);

        partSpawner = new PartSpawner(grid);
        partSpawner.FirstSpawn(startingPart);
    }

    public void StartGame()
    {
        Random.seed = int.Parse(System.DateTime.Today.Year.ToString() + System.DateTime.Today.DayOfYear);
        if (player != null)
            Destroy(player.gameObject);
        if (DeathWall != null)
        {
            foreach (MovementBehaviour b in DeathWall.MovementBehaviours)
                b.Stop();
            Destroy(DeathWall.gameObject);
        }
        if (recorder.IsPlayingRecording)
            recorder.StopPlayingRecording();

        CreatePlayer();
        CreateDeathWall();
        GenerateWorld();

        if (GhostEntity != null)
        {                
            GhostEntity.Die();
            recorder.TouchEvent -= GhostEntity.MovementBehaviours[0].Stop;
            recorder.UntouchEvent -= GhostEntity.MovementBehaviours[0].Move;
            Destroy(GhostEntity.gameObject);
        }

        if (BestGhost != null)
        {
            GhostEntity = CreateGhost();
            recorder.PlayRecording(BestGhost, GhostEntity.transform);
        }
        

        canvas.enabled = false;
        recorder.StartRecording(player.transform);
    }

    public void CreatePlayer()
    {
        player = new GameObject("Player").AddComponent<Entity>();
        player.gameObject.layer = 10;
        player.InitializeLists();

        //Components
        SpriteRenderer renderer = player.gameObject.AddComponent<SpriteRenderer>();
        renderer.sprite = playerSprite;
        renderer.sortingOrder = 1;
        CircleCollider2D collider = player.gameObject.AddComponent<CircleCollider2D>();
        collider.radius = 0.25f;
        Rigidbody2D rBody = player.gameObject.AddComponent<Rigidbody2D>();
        rBody.gravityScale = 2;
        rBody.drag = 1;
        rBody.freezeRotation = true;

        //Behaviours
        player.MovementBehaviours.Add(new ForceMovementBehaviour(player.gameObject, 30*60, Vector2.up, true));
        player.MovementBehaviours.Add(new ForceMovementBehaviour(player.gameObject, 10*60, Vector2.right, true));
        player.RotationBehaviours.Add(costantRotationBehaviour = new FaceVelocityRotationBehaviour(player.gameObject, true));

        //Others
        camera.target = (player.transform);

        //Events
        input.TouchBeginEvent += player.MovementBehaviours[0].Move;        
        input.TouchEndEvent += player.MovementBehaviours[0].Stop;
        player.DieEvent += PlayerKilled;

        //Initialize
        player.MovementBehaviours[1].Move();
        player.RotationBehaviours[0].Rotate();
    }

    public Entity CreateGhost()
    {
        Entity ghost = new GameObject("Ghost").AddComponent<Entity>();
        ghost.gameObject.layer = 10;
        ghost.InitializeLists();

        //Components
        SpriteRenderer renderer = ghost.gameObject.AddComponent<SpriteRenderer>();
        renderer.sprite = playerSprite;
        renderer.sortingOrder = 1;
        renderer.color = Color.gray;
        CircleCollider2D collider = ghost.gameObject.AddComponent<CircleCollider2D>();
        collider.radius = 0.25f;
        Rigidbody2D rBody = ghost.gameObject.AddComponent<Rigidbody2D>();
        rBody.gravityScale = 2;
        rBody.drag = 1;
        rBody.freezeRotation = true;

        //Behaviours
        ghost.MovementBehaviours.Add(new ForceMovementBehaviour(ghost.gameObject, 30*60, Vector2.up, true));
        ghost.MovementBehaviours.Add(new ForceMovementBehaviour(ghost.gameObject, 10*60, Vector2.right, true));
        ghost.RotationBehaviours.Add(costantRotationBehaviour = new FaceVelocityRotationBehaviour(ghost.gameObject, true));

        //Events
        recorder.UntouchEvent += ghost.MovementBehaviours[0].Stop;
        recorder.TouchEvent += ghost.MovementBehaviours[0].Move; 

        //Initialize
        ghost.MovementBehaviours[1].Move();
        ghost.RotationBehaviours[0].Rotate();

        return ghost;
    }

    public void CreateEntity()
    {
        Entity entity = new GameObject("Entity").AddComponent<Entity>();
        entity.gameObject.layer = 10;
        entity.InitializeLists();
    }

    public void CreateDeathWall()
    {
        DeathWall = Instantiate<GameObject>(DeathWallPrefab).GetComponent<Entity>();
        DeathWall.InitializeLists();
        Rigidbody2D rBody = DeathWall.GetComponent<Rigidbody2D>();
        rBody.gravityScale = 0;
        rBody.drag = 1;
        DeathWall.MovementBehaviours.Add(new ForceMovementBehaviour(DeathWall.gameObject, 7*60, Vector2.right, true));
        DeathWall.transform.position = new Vector3(-5, 0, 0);
        DeathWall.MovementBehaviours[0].Move();
        //player.DieEvent += constantMovement.Stop;
    }

    private void Update()
    {
        if (player != null)
        {
            if (partSpawner.GetLastPart().transform.position.x - player.transform.position.x < spawnNextPartDistance)
                partSpawner.Spawn(parts[Random.Range(0, parts.Length)]);
            if (player.transform.position.x - DeathWall.transform.position.x > maxDeathWallDistance)
                DeathWall.transform.position = player.transform.position - new Vector3(maxDeathWallDistance, 0, 0);
            if (partSpawner.SpawnedParts.Count > maxActiveParts)
            {
                Part partToDie = partSpawner.SpawnedParts[0];
                partSpawner.SpawnedParts.RemoveAt(0);
                Destroy(partToDie.gameObject);
            }
            text_Distance.text = (int)player.transform.position.x + " m";
        }
    }

    public void PlayerKilled()
    {
        canvas.enabled = true;
        heading.text = "It was inevitable...";
        input.TouchBeginEvent -= player.MovementBehaviours[0].Move;
        input.TouchEndEvent -= player.MovementBehaviours[0].Stop;
        foreach (MovementBehaviour b in player.MovementBehaviours)
            b.Stop();
        recorder.StopRecording();        

        int playerDistance = (int)player.transform.position.x;
        if (PlayerPrefs.GetInt("HighScore-" + System.DateTime.Today.Year + "-" + System.DateTime.Today.DayOfYear) < playerDistance)
        {
            PlayerPrefs.SetInt("HighScore-" + System.DateTime.Today.Year + "-" + System.DateTime.Today.DayOfYear, playerDistance);
            text_BestDistanceToday.text = "Today's Best " + playerDistance + " m";
            BestGhost = recorder.GhostRecording;
        }
        if (PlayerPrefs.GetInt("HighScore") < playerDistance)
        {
            PlayerPrefs.SetInt("HighScore", playerDistance);
            text_BestDistance.text = "Best " + playerDistance + " m";
        }       
    }
}
