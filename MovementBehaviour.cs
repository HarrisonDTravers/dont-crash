﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementBehaviour
{
    protected GameObject GameObject { get; set; }
    public float Speed { get; set; }

    public MovementBehaviour(GameObject gameObject, float speed)
    {
        GameObject = gameObject;
        Speed = speed;
    }

    public abstract void Move();
    public abstract void Stop();
}

public class ForceMovementBehaviour : MovementBehaviour
{
    private Vector2 Direction { get; set; }
    private Rigidbody2D rBody { get; set; }
    private bool CanHold { get; set; }
    private bool IsMoving { get; set; }

    public ForceMovementBehaviour(GameObject gameObject, float speed, Vector2 direction, bool canHold) : base(gameObject, speed)
    {
        GameObject = gameObject;
        Direction = direction;
        CanHold = canHold;
        if ((rBody = gameObject.GetComponent<Rigidbody2D>()) == null)
        {
            rBody = gameObject.AddComponent<Rigidbody2D>();
        }
    }

    public override void Move()
    {
        if (!CanHold)
            rBody.AddForce(Direction * Speed);
        else
        {
            if (!IsMoving)
            {
                IsMoving = true;
                GameLoader.Instance.StartCoroutine(RecurringMovement());
            }
        }
    }

    public override void Stop()
    {
        IsMoving = false;
    }

    public IEnumerator RecurringMovement()
    {
        while (IsMoving)
        {
            rBody.AddForce(Direction * Speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}

public class TranslateMovementBehaviour : MovementBehaviour
{
    private Vector2 Direction { get; set; }
    private bool CanHold { get; set; }
    private bool MovementStopped { get; set; }

    public TranslateMovementBehaviour(GameObject gameObject, float speed, Vector2 direction, bool canHold) : base(gameObject, speed)
    {
        GameObject = gameObject;
        Direction = direction;
        CanHold = canHold;
    }

    public override void Move()
    {
        if (!CanHold)
            GameObject.transform.Translate(Direction * Speed);
        else
        {
            MovementStopped = false;
            GameLoader.Instance.StartCoroutine(RecurringMovement());
        }
    }

    public override void Stop()
    {
        MovementStopped = true;
    }

    public IEnumerator RecurringMovement()
    {
        while (!MovementStopped)
        {
            GameObject.transform.Translate(Direction * Speed);
            yield return new WaitForEndOfFrame();
        }
    }
}
