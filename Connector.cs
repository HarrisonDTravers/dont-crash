﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connector : MonoBehaviour
{
    public enum ConnectorTypeEnum { Start, Center, End }
    public ConnectorTypeEnum ConnectorType;
}
