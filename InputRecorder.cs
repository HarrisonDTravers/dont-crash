﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostRecording
{
    public GhostRecording()
    {
        TouchTimes = new List<float>();
        UntouchTimes = new List<float>();
        TouchPositions = new List<KeyValuePair<Vector2, Vector2>>();
        UntouchPositions = new List<KeyValuePair<Vector2, Vector2>>();
    }

    public List<float> TouchTimes { get; set; }
    public List<float> UntouchTimes { get; set; }
    public List<KeyValuePair<Vector2, Vector2>> TouchPositions { get; set; }
    public List<KeyValuePair<Vector2, Vector2>> UntouchPositions { get; set; }
}

public class InputRecorder
{
    public GhostRecording GhostRecording { get; set; }
    private Transform Target { get; set; }
    private Rigidbody2D TargetRBody { get; set; }
    private bool FirstTouch { get; set; }

    public bool IsRecording { get; set; }
    public float CurrentTime { get; set; }
    public bool IsPlayingRecording { get; set; }

    public delegate void del();
    public event del TouchEvent;
    public event del UntouchEvent;

    public void StartRecording(Transform target)
    {
        CurrentTime = 0;
        GhostRecording = new GhostRecording();
        Target = target;
        TargetRBody = Target.GetComponent<Rigidbody2D>();
        IsRecording = true;
        FirstTouch = true;
        CoroutineSingleton.Instance.StartCoroutine(Timer());
    }

    public void Touch()
    {
        if(!IsRecording)
        {
            Debug.Log("Trying to record touches without starting record");
        }
        else
        {
            if (FirstTouch)
                FirstTouch = false;
            GhostRecording.TouchTimes.Add(CurrentTime);
            GhostRecording.TouchPositions.Add(new KeyValuePair<Vector2, Vector2>(Target.position, TargetRBody.velocity));
        }
    }

    public void Untouch()
    {
        if (!IsRecording)
        {
            Debug.Log("Trying to record touches without starting record");
        }
        else
        {
            if (!FirstTouch)
            {
                GhostRecording.UntouchTimes.Add(CurrentTime);
                GhostRecording.UntouchPositions.Add(new KeyValuePair<Vector2, Vector2>(Target.position, TargetRBody.velocity));
            }
        }
    }

    public void StopRecording()
    {        
        IsRecording = false;
        if(GhostRecording.TouchTimes.Count > GhostRecording.UntouchTimes.Count)
        {
            GhostRecording.UntouchTimes.Add(CurrentTime);
            GhostRecording.UntouchPositions.Add(new KeyValuePair<Vector2, Vector2>(Target.position, TargetRBody.velocity));
        }
    }

    public IEnumerator Timer()
    {
        while(IsRecording)
        {
            CurrentTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    public void PlayRecording(GhostRecording ghostRecording, Transform target)
    {
        IsPlayingRecording = true;
        CoroutineSingleton.Instance.StartCoroutine(PlayRecordingEnum(ghostRecording, target));
    }

    public void StopPlayingRecording()
    {
        IsPlayingRecording = false;
    }

    public IEnumerator PlayRecordingEnum(GhostRecording ghostRecording, Transform target)
    {
        Rigidbody2D rbody = target.GetComponent<Rigidbody2D>();
        int listIndex = 0;
        int index = 0;
        float timer = 0;

        if (ghostRecording.TouchTimes.Count > 0)
        {
            while (ghostRecording.TouchTimes[0] > ghostRecording.UntouchTimes[0])
                ghostRecording.UntouchTimes.RemoveAt(0);

            while (IsPlayingRecording)
            {
                while (listIndex == 0)
                {
                    if (timer >= ghostRecording.TouchTimes[index])
                    {
                        TouchEvent?.Invoke();
                        target.position = ghostRecording.TouchPositions[index].Key;
                        rbody.velocity = ghostRecording.TouchPositions[index].Value;
                        listIndex = 1;
                    }
                    timer += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                while (listIndex == 1)
                {
                    if (timer >= ghostRecording.UntouchTimes[index])
                    {
                        UntouchEvent?.Invoke();
                        target.position = ghostRecording.UntouchPositions[index].Key;
                        rbody.velocity = ghostRecording.UntouchPositions[index].Value;
                        index++;
                        Debug.Log(index);
                        if (index >= ghostRecording.TouchTimes.Count)
                            IsPlayingRecording = false;
                        else
                            listIndex = 0;
                    }
                    timer += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }
}
