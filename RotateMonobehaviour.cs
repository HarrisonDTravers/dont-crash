﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMonobehaviour : MonoBehaviour
{
    public float speed;
    public bool isClockwise;
    public bool isRandomDirection;

    // Start is called before the first frame update
    void Start()
    {
        if (isRandomDirection)
            isClockwise = Random.Range(0, 2) > 0 ? true : false; 
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.position, Vector3.forward, speed * Time.deltaTime);
    }
}
